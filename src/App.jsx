import React, { useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import HomeScreen from './components/HomeScreen'
import Navbar from './components/Navbar'
import Content from './components/Content'
import Footer from './components/Footer'

const useStyles = makeStyles({
  app: {
    width: '100vw',
    height: '100vh',
    margin: 0,
    padding: 0,
  },
})

const App = () => {
  const classes = useStyles()

  useEffect(() => {
    document.body.style.cursor = "url('https://raw.githubusercontent.com/chenglou/react-motion/master/demos/demo8-draggable-list/cursor.png') 39 39, auto"
  }, [])
  
  return (
    <div className={classes.app}>
      <Navbar />
      <HomeScreen />
      <Content />
      <Footer />
    </div>
  )
}

export default App
