import React, { useRef, useState, useMemo, useCallback } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Canvas, useFrame, extend, useThree } from 'react-three-fiber'
import { useSpring, a } from 'react-spring/three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import * as THREE from 'three'

const useStyles = makeStyles({
  container: {
    zIndex: 1,
    height: '100%',
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFAF0',
  },
  textContainer: {},
})

extend({ OrbitControls })

const Controls = () => {
  const { camera, gl } = useThree()
  const orbitRef = useRef()

  useFrame(() => {
    orbitRef.current.update()
  })

  return (
    <orbitControls
      args={[camera, gl.domElement]}
      ref={orbitRef}
      enableZoom={false}
    />
  )
}

const Plane = () => (
  <mesh rotation={[-Math.PI / 2, 0, 0]} position={[0, -3, 0]} receiveShadow>
    <planeBufferGeometry attach={'geometry'} args={[100, 100]} />
    <meshPhysicalMaterial attach={'material'} color={'#FFFAF0'} />
  </mesh>
)

const Box = () => {
  const mesh = useRef(null)
  const [hovered, setHovered] = useState(false)
  const [active, setActive] = useState(false)
  const spring = useSpring({
    scale: active ? [1.5, 1.5, 1.5] : [1, 1, 1],
    color: hovered ? 'blue' : 'black'
  })

  useFrame(() => (mesh.current.rotation.x = mesh.current.rotation.y += 0.001))

  return (
    <a.mesh
      ref={mesh}
      onPointerOver={() => setHovered(true)}
      onPointerOut={() => setHovered(false)}
      onClick={() => setActive(!active)}
      scale={spring.scale}
      castShadow
    >
      <boxBufferGeometry attach={'geometry'} args={[3, 3, 3]} />
      <a.meshPhysicalMaterial attach={'material'} color={spring.color} />
    </a.mesh>
  )
}

function Particles({ count, mouse }) {
  const mesh = useRef()

  const dummy = useMemo(() => new THREE.Object3D(), [])
  // Generate some random positions, speed factors and timings
  const particles = useMemo(() => {
    const temp = []
    for (let i = 0; i < count; i++) {
      const t = Math.random() * 100
      const factor = 20 + Math.random() * 100
      const speed = 0.001
      const xFactor = -50 + Math.random() * 100
      const yFactor = -50 + Math.random() * 100
      const zFactor = -50 + Math.random() * 100
      temp.push({ t, factor, speed, xFactor, yFactor, zFactor, mx: 0, my: 0 })
    }
    return temp
  }, [count])
  // The innards of this hook will run every frame
  useFrame(() => {
    // Run through the randomized data to calculate some movement
    particles.forEach((particle, i) => {
      let { t, factor, speed, xFactor, yFactor, zFactor } = particle
      // There is no sense or reason to any of this, just messing around with trigonometric functions
      t = particle.t += speed / 2
      const a = Math.cos(t) + Math.sin(t * 1) / 10
      const b = Math.sin(t) + Math.cos(t * 2) / 10
      const s = Math.cos(t)
      particle.mx += (mouse.current[0] - particle.mx) * 0.01
      particle.my += (mouse.current[1] * -1 - particle.my) * 0.01
      // Update the dummy object
      dummy.position.set(
        (particle.mx / 10) * a + xFactor + Math.cos((t / 10) * factor) + (Math.sin(t * 1) * factor) / 10,
        (particle.my / 10) * b + yFactor + Math.sin((t / 10) * factor) + (Math.cos(t * 2) * factor) / 10,
        (particle.my / 10) * b + zFactor + Math.cos((t / 10) * factor) + (Math.sin(t * 3) * factor) / 10
      )
      dummy.scale.set(s / 20, s / 20, s / 20)
      dummy.rotation.set(s * 5, s * 5, s * 5)
      dummy.updateMatrix()
      // And apply the matrix to the instanced item
      mesh.current.setMatrixAt(i, dummy.matrix)
    })
    mesh.current.instanceMatrix.needsUpdate = true
  })
  return (
    <>
      <instancedMesh ref={mesh} args={[null, null, count]}>
        <dodecahedronBufferGeometry attach="geometry" args={[0.2, 0]} />
        <meshPhongMaterial attach="material" color="#050505" />
      </instancedMesh>
    </>
  )
}


const HomeScreen = () => {
  const classes = useStyles()
  const mouse = useRef([0, 0])
  const onMouseMove = useCallback(({ clientX: x, clientY: y }) => (mouse.current = [x - window.innerWidth / 2, y - window.innerHeight / 2]), [])

  return (
      <Canvas
        className={classes.container}
        colorManagement
        onMouseMove={onMouseMove}
        camera={{ position: [-5, 2, 10], fov: 20 }}
        onCreated={({ gl }) => {
          gl.shadowMap.enabled = true
          gl.shadowMap.type = THREE.PCFSoftShadowMap
        }}
      >
        <fog attach={'fog'} args={['#FFFAF0', 10, 15]}/>
        <ambientLight intensity={1} />
        <spotLight position={[0, 5, 10]} penumbra={1} castShadow />
        <Controls />
        <Box />
        <Plane />
        <Particles count={15000} mouse={mouse} />
      </Canvas>
  )
}

export default HomeScreen
