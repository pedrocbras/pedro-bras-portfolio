import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Images from '../assets/Images'

const useStyles = makeStyles({
  text: {
    zIndex: 2,
    position: 'absolute',
    top: '25%',
    left: '25%',
    height: 400,
    width: 600,
    backgroundImage: `url(${Images.introText})`,
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
  },
})

const Content = () => {
  const classes = useStyles()
  return (
      <div className={classes.text} />
  )
}

export default Content
