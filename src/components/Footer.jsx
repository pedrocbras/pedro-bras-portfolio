import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import LinkedInIcon from '@material-ui/icons/LinkedIn'
import ColorLensIcon from '@material-ui/icons/ColorLens'
import GitHubIcon from '@material-ui/icons/GitHub'
import CodeIcon from '@material-ui/icons/Code'

const useStyles = makeStyles({
  footerContainer: {
    zIndex: 2,
    position: 'absolute',
    bottom: 0,
    right: 10,
    display: 'flex',
    alignItems: 'center'
  },
  link: {
    textDecoration: 'none',
    color: 'black !important',
    marginRight: 5,
  }
})

const Footer = () => {
  const classes = useStyles()
  return (
    <div className={classes.footerContainer}>
      <div>
        <a className={classes.link} target="_blank" rel="noopener noreferrer" href={'https://www.linkedin.com/in/pedrogcbras/'}>
          <LinkedInIcon />
        </a>
      </div>
      <div>
        <a className={classes.link} target="_blank" rel="noopener noreferrer" href={'https://github.com/pedbr'}>
          <GitHubIcon fontSize={'small'} />
        </a>
      </div>
      <div>
        <a className={classes.link} target="_blank" rel="noopener noreferrer" href={'https://bitbucket.org/pedrocbras/'}>
          <CodeIcon />
        </a>
      </div>
      <div>
        <a className={classes.link} target="_blank" rel="noopener noreferrer" href={'https://www.behance.net/pedro_bras'}>
          <ColorLensIcon />
        </a>
      </div>
    </div>
  )
}

export default Footer