import React, { useState } from 'react'
import { makeStyles, Button, Dialog, Typography, IconButton, DialogTitle, DialogContent } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'

const useStyles = makeStyles(theme => ({
  container: {
    zIndex: 2,
    width: '100%',
    position: 'absolute',
    display: 'flex',
    justifyContent: 'space-between',
  },
  routesContainer: {
    display: 'flex',
  },
  routeItem: {
    fontFamily: 'Krona One',
    fontSize: 12,
    margin: 10,
    '&:hover': {
      textDecoration: 'line-through',
    },
  },
  logo: {
    margin: 10,
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  dialogTitleRoot: {
    margin: 0,
    padding: theme.spacing(2),
  },
}))

const ContactDialog = ({ handleClose, open }) => {
  const classes = useStyles()
  return (
    <div>
      <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
        <DialogTitle disableTypography className={classes.dialogTitleRoot}>
          <Typography variant="h6">Coming Soon</Typography>
          {handleClose ? (
            <IconButton aria-label="close" className={classes.closeButton} onClick={handleClose}>
              <CloseIcon />
            </IconButton>
          ) : null}
        </DialogTitle>
        <DialogContent dividers>
          <Typography gutterBottom>
            I am currently developing new and awesome ways to showcase my information and my projects, stay tunned!
          </Typography>
        </DialogContent>
      </Dialog>
    </div>
  );
}

const Navbar = () => {
  const classes = useStyles()

  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  return (
    <>
    <div className={classes.container}>
      <div className={classes.logo} />
      <div className={classes.routesContainer}>
        <Button className={classes.routeItem} onClick={handleClickOpen}>ABOUT</Button>
        <Button className={classes.routeItem} onClick={handleClickOpen}>PROJECTS</Button>
        <Button variant={'outlined'} className={classes.routeItem} href="mailto:pedro.c.bras@gmail.com">REACH OUT</Button>
      </div>
    </div>
    <ContactDialog open={open} handleClose={handleClose} />
    </>
  )
}

export default Navbar
