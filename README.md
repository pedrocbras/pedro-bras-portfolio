# Pedro Brás Portfolio

**Here all the stack, tools, dependencies and scripts used to build this app, as well as the process and development description will be documented.**

---

## Stack

Technologies used.

1. The app is built entirely with the JavaScript language using the **MERN** stack, meaning the back-end will be built with **NodeJS**, **ExpressJS** and **MongoDB**, and **ReactJS** in the Front-End.

2. Compiling of the code is handled by **Webpack** and **Babel**

3. The Git and remote Repository supplier is **Bitbucket**, and this service is also be used for the building of the CI and CD pipelines.

4. The management of the project is made through **Jira** using the **Agile** Methodology.

5. For testing the tools used are **Cypress** for automated end-to-end testing, **React-testing-library** for Front-end Integration testing, and with **Jest** and **Enzyme** for Integration and Unit testing across the entire App.

6. State Management in the Front-end is handled with **Redux**

7. The styling of the app is made using **JSS** and their style objects, also using a predefined Theme.

8. Continuous Integrations and Continuous Deployment are handled directly in the **Bitbucket Pipelines** with the deployment hosted in **Netlify**

---
